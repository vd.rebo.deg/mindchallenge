export type AmplifyDependentResourcesAttributes = {
  "api": {
    "mindChallengeApi": {
      "ApiId": "string",
      "ApiName": "string",
      "RootUrl": "string"
    }
  },
  "auth": {
    "cognito8c8d0931": {
      "AppClientID": "string",
      "AppClientIDWeb": "string",
      "CreatedSNSRole": "string",
      "IdentityPoolId": "string",
      "IdentityPoolName": "string",
      "UserPoolArn": "string",
      "UserPoolId": "string",
      "UserPoolName": "string"
    }
  },
  "custom": {
    "documentDbCluster": {
      "ClusterEndpoint": "string",
      "ClusterId": "string",
      "ClusterPort": "string",
      "EngineVersion": "string"
    },
    "vpc": {
      "DocumentDBSecurityGroup": "string",
      "LambdaSecurityGroup": "string",
      "NoIngressSecurityGroup": "string",
      "PrivateSubnet1": "string",
      "PrivateSubnet2": "string",
      "PrivateSubnet3": "string",
      "PrivateSubnetGroup": "string",
      "PrivateSubnets": "string",
      "PublicSubnet1": "string",
      "PublicSubnet2": "string",
      "PublicSubnets": "string",
      "VPC": "string"
    }
  },
  "function": {
    "accountCrud": {
      "Arn": "string",
      "LambdaExecutionRole": "string",
      "LambdaExecutionRoleArn": "string",
      "Name": "string",
      "Region": "string"
    },
    "mindchallengebackenddocumentDbUtilities": {
      "Arn": "string"
    },
    "mindchallengebackendjoi": {
      "Arn": "string"
    },
    "mindchallengebackenduuid": {
      "Arn": "string"
    },
    "userCrud": {
      "Arn": "string",
      "LambdaExecutionRole": "string",
      "LambdaExecutionRoleArn": "string",
      "Name": "string",
      "Region": "string"
    }
  }
}