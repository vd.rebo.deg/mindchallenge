/*
Use the following code to retrieve configured secrets from SSM:

const aws = require('aws-sdk');

const { Parameters } = await (new aws.SSM())
  .getParameters({
    Names: ["documentDBUser","documentDBPassword"].map(secretName => process.env[secretName]),
    WithDecryption: true,
  })
  .promise();

Parameters will be of the form { Name: 'secretName', Value: 'secretValue', ... }[]
*/
/* Amplify Params - DO NOT EDIT
	ENV
	REGION
Amplify Params - DO NOT EDIT */

const aws = require("aws-sdk");
const { connect } = require("/opt/nodejs/dbUtils");
const handlers = require("./handlers");

const { DB_ENDPOINT, DB_PORT } = process.env;

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */
exports.handler = async (event) => {
  console.log(`EVENT: ${JSON.stringify(event)}`);

  const { Parameters } = await new aws.SSM()
    .getParameters({
      Names: ["documentDBUser", "documentDBPassword"].map(
        (secretName) => process.env[secretName]
      ),
      WithDecryption: true,
    })
    .promise();

  const dbUser = Parameters.find(
    (item) => item.Name === process.env["documentDBUser"]
  ).Value;
  const dbPassword = Parameters.find(
    (item) => item.Name === process.env["documentDBPassword"]
  ).Value;

  await connect({
    user: dbUser,
    password: dbPassword,
    endpoint: DB_ENDPOINT,
    port: DB_PORT,
  });

  let result = {};

  try {
    switch (event.httpMethod) {
      case "POST": {
        const { resource } = event;
        if (resource === "/v1/account") {
          result = await handlers.handlePost(event);
        } else if (resource === "/v1/account/{accountID}/team") {
          result = await handlers.handlePostTeamMembers(event);
        }
        break;
      }
      case "PUT":
        result = await handlers.handlePut(event);
        break;
      case "DELETE": {
        const { resource } = event;
        if (resource === "/v1/account/{accountID}/team") {
          result = await handlers.handleDeleteTeamMembers(event);
        } else {
          result = await handlers.handleDelete(event);
        }
        break;
      }
      case "GET": {
        const { resource } = event;
        if (resource === "/v1/account") {
          result = await handlers.handleGet(event);
        } else if (resource === "/v1/account/{accountID}/team") {
          result = await handlers.handleGetTeam(event);
        } else {
          result = await handlers.handleGetById(event);
        }
        break;
      }
      default:
    }
  } catch (error) {
    console.error(error);
    if (
      error.name === "InvalidParameterException" ||
      error.name === "ValidationError"
    ) {
      return {
        statusCode: 400,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "*",
        },
        body: JSON.stringify(error.message),
      };
    }
  }

  return {
    statusCode: 200,
    //  Uncomment below to enable CORS requests
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "*",
    },
    body: JSON.stringify(result),
  };
};
