const { User } = require("/opt/nodejs/mongoose/models");
const accountCrud = require("/opt/nodejs/cruds/account");
const { not } = require("../utils/set");

async function handleDelete(event) {
  const { accountID } = event.pathParameters;

  // Update user data
  return accountCrud.deleteById(accountID);
}

async function handleDeleteTeamMembers(event) {
  const { accountID } = event.pathParameters;
  const { membersToRemove } = JSON.parse(event.body);
  // Consulta de equipo actual
  let currentTeam = (await accountCrud.findTeam(accountID)).team || [];
  currentTeam = currentTeam.map(member => member._id.toString());

  // Consulta de referencias de miembros a eliminar
  const usersToRemove = await Promise.all(membersToRemove.map(async (memberID) => {
    let user = await User.findOne(
      { id: memberID, deleted: { $ne: true } },
      "id _id"
    );
    return user._id.toString();
  }));

  // Filtrar miembros a eliminar
  const newTeam = not(currentTeam, usersToRemove);

  console.log({ currentTeam });
  console.log({ usersToRemove });
  console.log({ newTeam });

   // Guardar resultado
   const updatedAccount = await accountCrud.updateById(accountID, {
    team: newTeam,
  });

  // Devolver nuevo arreglo de miembros de equipo
  return updatedAccount.team;
}

module.exports = { handleDelete, handleDeleteTeamMembers };
