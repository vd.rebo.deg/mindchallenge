const { v4: uuidv4 } = require("uuid");
const accountCrud = require("/opt/nodejs/cruds/account");
const { User } = require("/opt/nodejs/mongoose/models");
const { postAccountSchema } = require("../validator/schemas");
const { union } = require("../utils/set");

async function handlePost(event) {
  // Transformar datos de entrada
  const data = JSON.parse(event.body);
  // Validar datos de entrada
  await postAccountSchema.validateAsync(data);
  // Registrar datos en BD
  const id = uuidv4();
  const newAccount = await accountCrud.save({ id, ...data });
  return newAccount;
}

async function handlePostTeamMembers(event) {
  const { accountID } = event.pathParameters;
  const { newMembers } = JSON.parse(event.body);
  // Consulta de equipo actual
  let currentTeam = (await accountCrud.findTeam(accountID)).team || [];
  currentTeam = currentTeam.map(member => member._id.toString());

  // Consulta de referencias de nuevos miembros
  const usersToAdd = await Promise.all(newMembers.map(async (memberID) => {
    let user = await User.findOne(
      { id: memberID, deleted: { $ne: true } },
      "id _id"
    );
    return user._id.toString();
  }));

  // Mezclar ambos arreglos
  const newTeam = union(currentTeam, usersToAdd);

  console.log({ currentTeam }); 
  console.log({ usersToAdd });
  console.log({ newTeam });

  // Guardar resultado
  const updatedAccount = await accountCrud.updateById(accountID, {
    team: newTeam,
  });

  // Devolver nuevo arreglo de miembros de equipo
  return updatedAccount.team;
}

module.exports = { handlePost, handlePostTeamMembers };
