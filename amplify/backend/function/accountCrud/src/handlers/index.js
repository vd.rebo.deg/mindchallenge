const {handlePost, handlePostTeamMembers} = require("./post");
const handlePut = require("./put");
const { handleGet, handleGetById, handleGetTeam } = require("./get");
const {handleDelete, handleDeleteTeamMembers} = require("./delete");

module.exports = {
  handlePost,
  handlePostTeamMembers,
  handlePut,
  handleGet,
  handleGetById,
  handleGetTeam,
  handleDelete,
  handleDeleteTeamMembers
};
