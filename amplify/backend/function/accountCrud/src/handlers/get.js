const { Account } = require("/opt/nodejs/mongoose/models");
const accountCrud = require("/opt/nodejs/cruds/account");

async function handleGet(event) {
  const result = await Account.find({ deleted: { $ne: true } });
  return result;
}

async function handleGetById(event) {
  const { accountID } = event.pathParameters;
  // Validar datos de entrada
  const result = await Account.findOne({ id: accountID, deleted: { $ne: true } });
  // Mandar mensaje apropiado en caso de que no exista el recurso
  return result;
}

async function handleGetTeam(event) {
  const { accountID } = event.pathParameters;
  // Validar datos de entrada
  
  const result = await accountCrud.findTeam(accountID);
  // Mandar mensaje apropiado en caso de que no exista el recurso
  return result;
}

module.exports = { handleGet, handleGetById, handleGetTeam };
