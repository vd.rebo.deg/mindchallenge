const accountCrud = require("/opt/nodejs/cruds/account");
const { putAccountSchema } = require("../validator/schemas");

async function handlePut(event) {
  // Transform input data
  const { accountID } = event.pathParameters;
  const data = JSON.parse(event.body);
  // Validate input data
  await putAccountSchema.validateAsync(data);
  // Update account data
  const updatedAccount = await accountCrud.updateById(accountID, data);
  return updatedAccount;
}

module.exports = handlePut;
