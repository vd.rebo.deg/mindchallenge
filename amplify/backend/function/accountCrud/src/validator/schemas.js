const Joi = require("joi");

const postAccountSchema = Joi.object({
  accountName: Joi.string().required(),
  clientName: Joi.string().required(),
  operationsManager: Joi.string().required(),
});

const putAccountSchema = Joi.object({
  accountName: Joi.string(),
  clientName: Joi.string(),
  operationsManager: Joi.string(),
});

module.exports = {
  postAccountSchema,
  putAccountSchema,
};
