const { User } = require("../mongoose/models");

async function save(userData) {
  const newUser = new User({ ...userData, updatedAt: new Date() });
  const result = await newUser.save();
  return result;
}

async function updateById(id, data) {
  const userUpdated = User.findOneAndUpdate(
    { id, deleted: {$ne: true} },
    { ...data, updatedAt: new Date() },
    { new: true }
  );
  return userUpdated;
}

async function deleteById(id) {
  const userUpdated = User.findOneAndUpdate(
    { id, deleted: {$ne: true} },
    { deleted: true, updatedAt: new Date() },
    { new: true }
  );
  return userUpdated;
}

module.exports = {
  save,
  updateById,
  deleteById
};
