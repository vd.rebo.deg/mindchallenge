const { Account } = require("../mongoose/models");

async function save(accountData) {
  const newAccount = new Account({ ...accountData, updatedAt: new Date() });
  const result = await newAccount.save();
  return result;
}

async function updateById(id, data) {
  const accountUpdated = Account.findOneAndUpdate(
    { id, deleted: {$ne: true} },
    { ...data, updatedAt: new Date() },
    { new: true }
  );
  return accountUpdated;
}

async function deleteById(id) {
  const accountUpdated = Account.findOneAndUpdate(
    { id, deleted: {$ne: true} },
    { deleted: true, updatedAt: new Date() },
    { new: true }
  );
  return accountUpdated;
}

async function findTeam(id) {
  const result = await Account.findOne({ id, deleted: { $ne: true } }, "id _id team").populate("team");
  return result;
}

module.exports = {
  save,
  updateById,
  deleteById,
  findTeam
};
