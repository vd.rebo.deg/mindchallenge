const mongoose = require("mongoose");

let client;
var MongoClient = require("mongodb").MongoClient;

async function getClient({ user, password, endpoint, port }) {
  if (!client) {
    return await MongoClient.connect(
      `mongodb://${user}:${password}@${endpoint}:${port}/?tls=true&replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false`,
      {
        tlsCAFile: `/opt/certs/rds-combined-ca-bundle.pem`, //Specify the DocDB; cert
      }
    );
  }

  return client;
}

async function connect({ user, password, endpoint, port }) {
  return await mongoose.connect(
    `mongodb://${user}:${password}@${endpoint}:${port}/?tls=true&replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false`,
    {
      sslCA: `/opt/certs/rds-combined-ca-bundle.pem`, //Specify the DocDB; cert
    }
  );
}

module.exports = {
  getClient,
  connect,
};
