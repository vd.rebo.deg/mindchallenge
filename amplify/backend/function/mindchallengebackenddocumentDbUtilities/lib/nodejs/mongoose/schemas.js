const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  id: {
    type: String,
    index: true,
    required: true,
    immutable: true,
    unique: true,
  },
  name: String,
  roles: [String],
  email: { type: String, index: true, required: true, immutable: true },
  englishLevel: String,
  hardSkills: String,
  cvLink: String,
  deleted: { type: Boolean, required: false, default: false},
  updatedAt: { type: Date, required: true },
  createdAt: { type: Date, required: true, default: Date.now },
});

const accountSchema = new mongoose.Schema({
  id: {
    type: String,
    index: true,
    required: true,
    immutable: true,
    unique: true,
  },
  accountName: { type: String, required: true },
  clientName: { type: String, required: true },
  operationsManager: { type: String, required: true },
  team: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  deleted: { type: Boolean, required: false, default: false},
  updatedAt: { type: Date, required: true },
  createdAt: { type: Date, required: true, default: Date.now },
});

module.exports = {
  userSchema,
  accountSchema
};
