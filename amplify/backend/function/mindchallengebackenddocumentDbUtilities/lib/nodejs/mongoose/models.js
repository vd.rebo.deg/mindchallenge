const mongoose = require("mongoose");
const { userSchema, accountSchema } = require("./schemas");

const User = new mongoose.model("User", userSchema);
const Account = new mongoose.model("Account", accountSchema);

module.exports = {
  User,
  Account,
};
