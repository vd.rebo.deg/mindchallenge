const aws = require("aws-sdk");

const { CognitoIdentityServiceProvider } = aws;

const { AUTH_COGNITO8C8D0931_USERPOOLID } = process.env;
const cognitoService = new CognitoIdentityServiceProvider();

async function createUser(userData) {
  const params = {
    UserPoolId: AUTH_COGNITO8C8D0931_USERPOOLID,
    TemporaryPassword: userData.password,
    Username: userData.email,
    UserAttributes: [
      {
        Name: "email",
        Value: userData.email,
      },
      {
        Name: "name",
        Value: userData.name,
      },
    ],
  };

  const { User } = await cognitoService.adminCreateUser(params).promise();

  return {
    ...userData,
    id: User.Username,
  };
}

module.exports = {
  createUser,
};
