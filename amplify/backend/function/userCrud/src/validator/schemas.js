const Joi = require("joi");

const postUserSchema = Joi.object({
  name: Joi.string(),
  password: Joi.string()
    .pattern(new RegExp("^[a-zA-Z0-9!@#$%^&*)(+=._-]{8,20}$"))
    .min(8)
    .max(20)
    .required(),
  email: Joi.string().email().required(),
  roles: Joi.array().items(Joi.string().valid(...["ADMIN", "SADMIN", "GENERAL"])),
  englishLevel: Joi.string(),
  hardSkills: Joi.string(),
  cvLink: Joi.string(),
});

const putUserSchema = Joi.object({
  id: Joi.string().guid(),
  name: Joi.string(),
  email: Joi.string().email(),
  englishLevel: Joi.string(),
  hardSkills: Joi.string(),
  roles: Joi.array().items(Joi.string().valid(...["ADMIN", "SADMIN", "GENERAL"])),
  cvLink: Joi.string(),
});

module.exports = {
  postUserSchema,
  putUserSchema,
};
