const handlePost = require("./post");
const handlePut = require("./put");
const { handleGet, handleGetById } = require("./get");
const handleDelete = require("./delete");

module.exports = {
  handlePost,
  handlePut,
  handleGet,
  handleGetById,
  handleDelete,
};
