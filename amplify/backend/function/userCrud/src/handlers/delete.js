const userCrud = require("/opt/nodejs/cruds/user");

async function handleDelete(event) {
  const { userID } = event.pathParameters;

  // Update user data
  return userCrud.deleteById(userID);
}

module.exports = handleDelete;
