const { User } = require("/opt/nodejs/mongoose/models");

async function handleGet(event) {
  const result = await User.find({ deleted: { $ne: true } });
  return result;
}

async function handleGetById(event) {
  const { userID } = event.pathParameters;
  // Validar datos de entrada
  const result = await User.findOne({ id: userID, deleted: { $ne: true } });
  // Mandar mensaje apropiado en caso de que no exista el recurso
  return result;
}

module.exports = { handleGet, handleGetById };
