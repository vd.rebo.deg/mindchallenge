const userCrud = require("/opt/nodejs/cruds/user");
const cognitoUtils = require("../aws/cognito");
const { postUserSchema } = require("../validator/schemas");

async function createUser(userData) {
  // Crear usuario en Cognito
  const cognitoUser = await cognitoUtils.createUser(userData);

  // Registrar usuario en DocumentDB
  const recordedUser = await userCrud.save(cognitoUser);

  return recordedUser;
}

async function handlePost(event) {
  // Transformar datos de entrada
  const data = JSON.parse(event.body);
  // Validar datos de entrada
  await postUserSchema.validateAsync(data);
  return createUser(data);
}

module.exports = handlePost;
