const userCrud = require("/opt/nodejs/cruds/user");
const { putUserSchema } = require("../validator/schemas");

async function handlePut(event) {
  // Transform input data
  const data = JSON.parse(event.body);
  // Validate input data
  await putUserSchema.validateAsync(data);
  // Update user data
  const updatedUser = await userCrud.updateById(data.id, data);
  return updatedUser;
}

module.exports = handlePut;
