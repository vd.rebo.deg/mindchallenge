import { useEffect } from "react";
import { Amplify } from "aws-amplify";
import { withAuthenticator, useAuthenticator } from "@aws-amplify/ui-react";
import { useDispatch } from "react-redux";
import MainLayout from "./layout/MainLayout";
import "@aws-amplify/ui-react/styles.css";
import awsExports from "./aws-exports";
import Root from "routes/root";
import { setUser } from "redux/user";
import fetchUser from "api/fetchUser";

Amplify.configure(awsExports);

function App() {
  const { authStatus } = useAuthenticator((context) => [context.authStatus]);
  const { user } = useAuthenticator((context) => [context.user]);
  const dispatch = useDispatch();

  useEffect(() => {
    if (authStatus === "authenticated") {
      setProfileData(user.username);
    }
  }, [authStatus]);

  async function setProfileData(userID) {
    const profileData = await fetchUser(userID);
    const user = {
      id: profileData.id,
      email: profileData.email,
      name: profileData.name,
      roles: [...profileData.roles],
    };
    dispatch(setUser(user));
  }

  return (
    <MainLayout>
      <Root />
    </MainLayout>
  );
}

export default withAuthenticator(App, {
  loginMechanisms: ["email"],
  hideSignUp: true,
});
