import { Routes, Route } from "react-router-dom";
import UserDetails from "components/admin/UserDetails";
import UsersManagement from "components/admin/UsersManagement";
import AccountsManagement from "components/admin/account/AccountsManagement";
import AccountDetails from "components/admin/account/AccountDetails";


function AdminRoutes() {
  return (
    <Routes>
      <Route path="usuarios" element={<UsersManagement />} />
      <Route path="usuarios/:userID" element={<UserDetails />} />
      <Route
        path="cuentas"
        element={<AccountsManagement />
      }
      />
      <Route path="cuentas/:accountID" element={<AccountDetails />} />
      <Route
        path="movimientos"
        element={
          <>
            <div>Movimientos</div>
          </>
        }
      />
    </Routes>
  );
}

function Root() {
  return (
    <Routes>
      <Route exact path="/">
        Bienvenido
      </Route>
      <Route path="/admin/*" element={<AdminRoutes />} />
      <Route path="/perfil" element={<UserDetails profile />} />
    </Routes>
  );
}

export default Root;
