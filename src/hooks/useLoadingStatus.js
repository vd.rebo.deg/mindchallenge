import { useState, useCallback, useMemo } from "react";

function useLoadingStatus(promise) {
  const [loading, setLoading] = useState(false);

  const task = useCallback(
    (...args) => {
      setLoading(true);
      return promise(...args).then(
        (result) => {
          setLoading(false);
          return result;
        },
        (error) => {
          setLoading(false);
          throw error;
        }
      );
    },
    [promise]
  );

  return useMemo(() => [loading, task], [loading, task]);
}

export default useLoadingStatus;
