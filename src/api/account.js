import { API } from "aws-amplify";

export async function fetch(accountID) {
  return await API.get("mindChallengeApi", `/v1/account/${accountID}`);
}

export async function del(accountID) {
  return API.del("mindChallengeApi", `/v1/account/${accountID}`);
}

export async function create(accountData) {
  const data = {
    body: accountData,
  };
  return API.post("mindChallengeApi", "/v1/account", data);
}

export async function update(accountID, accountData) {
  const data = {
    body: accountData,
  };
  return API.put("mindChallengeApi", `/v1/account/${accountID}`, data);
}

export async function fetchTeam(accountID) {
  return API.get("mindChallengeApi", `/v1/account/${accountID}/team`);
}

export async function assignTeamUsers(accountID, usersIDs = []) {
  const data = {
    body: {
      newMembers: usersIDs,
    },
  };
  return API.post("mindChallengeApi", `/v1/account/${accountID}/team`, data);
}

export async function removeTeamUsers(accountID, usersIDs = []) {
  const data = {
    body: {
      membersToRemove: usersIDs,
    },
  };
  return API.del("mindChallengeApi", `/v1/account/${accountID}/team`, data);
}
