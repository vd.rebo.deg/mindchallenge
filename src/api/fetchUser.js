import { API } from "aws-amplify";

async function fetchUser(userID) {
  return await API.get("mindChallengeApi", `/v1/user/${userID}`);
}

export default fetchUser;