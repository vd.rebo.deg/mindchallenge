import { API } from "aws-amplify";


export async function fetchAll() {
  return await API.get("mindChallengeApi", "/v1/user");
}
