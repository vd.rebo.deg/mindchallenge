export default function notEmptyString(
  string = "",
  wildcard = "Sin información"
) {
  string = !string ? "" : string.trim();
  return string === "" ? wildcard : string;
}
