import { Auth } from "aws-amplify";

export async function signOut() {
  return Auth.signOut();
}

