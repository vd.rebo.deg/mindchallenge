
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import DialogActions from "@mui/material/DialogActions";
import Button from "@mui/material/Button";
import nop from "utils/nop";


function ConfirmationDialog(props) {
  const { open, ...other } = props;

  const handleCancel = () => {
    props.onCancel();
  };

  const handleOk = () => {
    props.onConfirm();
  };

  return (
    <Dialog
      sx={{ "& .MuiDialog-paper": { width: "80%", maxHeight: 435 } }}
      maxWidth="xs"
      open={open}
      {...other}
    >
      <DialogTitle>Confirmación</DialogTitle>
      <DialogContent dividers>
        ¿Está seguro que desea realizar esta operación?
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={handleCancel}>
          Cancelar
        </Button>
        <Button onClick={handleOk}>Confirmar</Button>
      </DialogActions>
    </Dialog>
  );
}

ConfirmationDialog.defaultProps = {
  onConfirm: nop,
  onCancel: nop,
};


export default ConfirmationDialog;