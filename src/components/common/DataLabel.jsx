import Typography from "@mui/material/Typography";
import Skeleton from "@mui/material/Skeleton";

function DataLabel(props) {
  if (props.loading) {
    return (
      <>
        <Skeleton variant="text" width={100} />
        <Skeleton variant="text" width={200} />
      </>
    );
  }
  return (
    <>
      <Typography variant="subtitle2">{props.title}</Typography>
      <Typography variant="body2">{props.data}</Typography>
    </>
  );
}

export default DataLabel;
