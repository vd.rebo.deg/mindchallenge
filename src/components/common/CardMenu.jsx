import ListItemText from "@mui/material/ListItemText";
import ListItemIcon from "@mui/material/ListItemIcon";
import Menu from "@mui/material/Menu";
import MenuList from "@mui/material/MenuList";
import MenuItem from "@mui/material/MenuItem";
import Divider from "@mui/material/Divider";

function CardMenu(props) {
    return (
      <Menu
        id="basic-menu"
        anchorEl={props.anchorEl}
        open={props.open}
        onClose={props.onClose}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
      >
        <MenuList>
          {props.actions.map((action) => (
            <div>
              <MenuItem
                onClick={() => {
                  props.onClose();
                  action.action();
                }}
              >
                {action.icon && <ListItemIcon>{action.icon}</ListItemIcon>}
                <ListItemText>{action.text}</ListItemText>
              </MenuItem>
              {action.divider && <Divider />}
            </div>
          ))}
        </MenuList>
      </Menu>
    );
  }

  export default CardMenu;