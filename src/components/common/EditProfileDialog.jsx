import React, { useEffect } from "react";
import { API } from "aws-amplify";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import { useFormik } from "formik";

import useLoadingStatus from "hooks/useLoadingStatus";
import { updateProfileSchema } from "validator/yup/schemas";
import nop from "utils/nop";

async function updateUser(userData) {
  const data = {
    body: userData,
  };
  return await API.put("mindChallengeApi", "/v1/user", data);
}

function EditProfileDialog(props) {
  const [updateLoading, _updateUser] = useLoadingStatus(updateUser);
  const formik = useFormik({
    initialValues: {
      name: "",
      englishLevel: "",
      cvLink: "",
      hardSkills: "",
    },
    validationSchema: updateProfileSchema,
    onSubmit: async ({ role, ...values }, { resetForm }) => {
      let data = {
        id: props.user.id,
        ...values,
      };

      const userUpdated = await _updateUser(data);
      props.onUserUpdated(userUpdated);

      resetForm();
    },
  });

  useEffect(() => {
    if (props.open) {
      const userToUpdate = {
        name: props.user.name,
        englishLevel: props.user.englishLevel,
        cvLink: props.user.cvLink,
        hardSkills: props.user.hardSkills,
      };

      formik.setValues(userToUpdate);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.userID, props.open]);

  function handleOnCancel() {
    formik.resetForm();
    props.onCancel();
  }

  const loading = updateLoading;

  return (
    <Dialog open={props.open} onClose={props.onCancel}>
      <form onSubmit={formik.handleSubmit}>
        <DialogTitle>Perfil</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Actualiza tu perfil con la información más reciente.
          </DialogContentText>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoFocus
                margin="dense"
                id="name"
                label="Nombre completo"
                type="text"
                fullWidth
                variant="standard"
                required
                value={formik.values.name}
                onChange={formik.handleChange}
                error={formik.touched.name && Boolean(formik.errors.name)}
                helperText={formik.touched.name && formik.errors.name}
              />
            </Grid>

            <Grid item xs={12} sm={6}>
              <FormControl
                variant="standard"
                sx={{ m: 1, minWidth: 120 }}
                fullWidth
                error={
                  formik.touched.englishLevel &&
                  Boolean(formik.errors.englishLevel)
                }
              >
                <InputLabel id="demo-simple-select-standard-label">
                  Nivel de Inglés
                </InputLabel>
                <Select
                  labelId="demo-simple-select-standard-label"
                  id="englishLevel"
                  name="englishLevel"
                  value={formik.values.englishLevel}
                  onChange={formik.handleChange}
                  label="Nivel de Inglés"
                >
                  <MenuItem value="BASIC">Básico</MenuItem>
                  <MenuItem value="INTERMEDIUM">Intermedio</MenuItem>
                  <MenuItem value="ADVANCE">Avanzado</MenuItem>
                </Select>
                <FormHelperText>
                  {formik.touched.englishLevel && formik.errors.englishLevel}
                </FormHelperText>
              </FormControl>
            </Grid>

            <Grid item xs={12}>
              <TextField
                autoFocus
                margin="dense"
                id="cvLink"
                label="Enlace de CV"
                name="cvLink"
                type="url"
                fullWidth
                variant="standard"
                value={formik.values.cvLink}
                onChange={formik.handleChange}
                error={formik.touched.cvLink && Boolean(formik.errors.cvLink)}
                helperText={formik.touched.cvLink && formik.errors.cvLink}
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                margin="dense"
                id="hardSkills"
                label="Conocimientos Técnicos"
                type="hardSkills"
                multiline
                rows={5}
                fullWidth
                variant="outlined"
                value={formik.values.hardSkills}
                onChange={formik.handleChange}
                error={
                  formik.touched.hardSkills && Boolean(formik.errors.hardSkills)
                }
                helperText={
                  formik.touched.hardSkills && formik.errors.hardSkills
                }
              />
            </Grid>
            <Grid item xs={12}>
              <FormHelperText>* Campos requeridos</FormHelperText>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleOnCancel} disabled={loading} type="reset">
            Cancelar
          </Button>
          <Box sx={{ m: 1, position: "relative" }}>
            <Button type="submit" disabled={loading}>
              ACTUALIZAR
            </Button>
            {loading && (
              <CircularProgress
                size={24}
                sx={{
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  marginTop: "-12px",
                  marginLeft: "-12px",
                }}
              />
            )}
          </Box>
        </DialogActions>
      </form>
    </Dialog>
  );
}

EditProfileDialog.defaultProps = {
  onUserUpdated: nop,
  userID: null,
  user: {},
};

export default EditProfileDialog;
