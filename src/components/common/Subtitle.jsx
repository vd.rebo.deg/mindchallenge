import Typography from "@mui/material/Typography";
import Skeleton from "@mui/material/Skeleton";

function Subtitle(props) {
  if (props.loading) {
    return <Skeleton variant="rounded" width={150} height={20} />;
  }

  return (
    <Typography variant={{ mb: 1.5 }} paragraph>
      {props.children}
    </Typography>
  );
}

export default Subtitle;
