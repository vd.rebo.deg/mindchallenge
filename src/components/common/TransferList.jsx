import * as React from "react";
import Grid from "@mui/material/Grid";
import List from "@mui/material/List";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemIcon from "@mui/material/ListItemIcon";
import Checkbox from "@mui/material/Checkbox";
import Button from "@mui/material/Button";
import Divider from "@mui/material/Divider";
import nop from "utils/nop";
import { intersection, not, union } from "utils/set";

function TransferList(props) {
  const [checked, setChecked] = React.useState([]);
  const [left, setLeft] = React.useState([]);
  const [right, setRight] = React.useState([]);
  const [itemsMap, setItemsMap] = React.useState({});

  const leftChecked = intersection(checked, left);
  const rightChecked = intersection(checked, right);

  React.useEffect(() => {
    let itemsMap = {};
    const currentIds = props.right.map((item) => {
      itemsMap = {
        ...itemsMap,
        [item.id]: item,
      };
      return item.id;
    });
    setRight(currentIds);
    setItemsMap((currentItemsMap) => ({ ...currentItemsMap, ...itemsMap }));
  }, [props.right]);

  React.useEffect(() => {
    let itemsMap = {};
    const optionsIds = props.left.map((item) => {
      itemsMap = {
        ...itemsMap,
        [item.id]: item,
      };
      return item.id;
    });
    setLeft(optionsIds);
    setItemsMap((currentItemsMap) => ({ ...currentItemsMap, ...itemsMap }));
  }, [props.left]);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const numberOfChecked = (items) => intersection(checked, items).length;

  const handleToggleAll = (items) => () => {
    if (numberOfChecked(items) === items.length) {
      setChecked(not(checked, items));
    } else {
      setChecked(union(checked, items));
    }
  };

  const handleCheckedRight = () => {
    let _right = right.concat(leftChecked);
    setRight(_right);
    setLeft(not(left, leftChecked));
    setChecked(not(checked, leftChecked));
    const selectedItems = _right.map((itemId) => itemsMap[itemId]);
    props.onChange(selectedItems);
  };

  const handleCheckedLeft = () => {
    let _right = not(right, rightChecked);
    setLeft(left.concat(rightChecked));
    setRight(_right);
    setChecked(not(checked, rightChecked));

    const selectedItems = _right.map((itemId) => itemsMap[itemId]);
    props.onChange(selectedItems);
  };

  const customList = (title, items) => (
    <Card>
      <CardHeader
        sx={{ px: 2, py: 1 }}
        avatar={
          <Checkbox
            onClick={handleToggleAll(items)}
            checked={
              numberOfChecked(items) === items.length && items.length !== 0
            }
            indeterminate={
              numberOfChecked(items) !== items.length &&
              numberOfChecked(items) !== 0
            }
            disabled={items.length === 0}
            inputProps={{
              "aria-label": "all items selected",
            }}
          />
        }
        title={title}
        subheader={`${numberOfChecked(items)}/${items.length} selected`}
      />
      <Divider />
      <List
        sx={{
          width: 300,
          height: 230,
          bgcolor: "background.paper",
          overflow: "auto",
        }}
        dense
        component="div"
        role="list"
      >
        {items.map((value) => {
          const labelId = `transfer-list-all-item-${value}-label`;
          const item = itemsMap[value];
          return (
            <ListItem
              key={value}
              role="listitem"
              button
              onClick={handleToggle(value)}
            >
              <ListItemIcon>
                <Checkbox
                  checked={checked.indexOf(value) !== -1}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{
                    "aria-labelledby": labelId,
                  }}
                />
              </ListItemIcon>
              <ListItemText
                id={labelId}
                primary={item.name}
                secondary={item.email}
              />
            </ListItem>
          );
        })}
      </List>
    </Card>
  );

  return (
    <Grid container spacing={2} justifyContent="center" alignItems="center">
      <Grid item>{customList("Choises", left)}</Grid>
      <Grid item>
        <Grid container direction="column" alignItems="center">
          <Button
            sx={{ my: 0.5 }}
            variant="outlined"
            size="small"
            onClick={handleCheckedRight}
            disabled={leftChecked.length === 0}
            aria-label="move selected right"
          >
            &gt;
          </Button>
          <Button
            sx={{ my: 0.5 }}
            variant="outlined"
            size="small"
            onClick={handleCheckedLeft}
            disabled={rightChecked.length === 0}
            aria-label="move selected left"
          >
            &lt;
          </Button>
        </Grid>
      </Grid>
      <Grid item>{customList("Chosen", right)}</Grid>
    </Grid>
  );
}

TransferList.defaultProps = {
  onChange: nop,
};

export default TransferList;
