import Typography from "@mui/material/Typography";
import Skeleton from "@mui/material/Skeleton";
import Box from "@mui/system/Box";

function Title(props) {
  if (props.loading) {
    return (
      <Box mb={1}>
        <Skeleton variant="rounded" width={300} height={40} />
      </Box>
    );
  }

  return (
    <Typography variant="h5" paragraph={props.paragraph}>
      {props.children}
    </Typography>
  );
}

export default Title;
