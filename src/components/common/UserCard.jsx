import { useState } from "react";

import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Grid from "@mui/material/Grid";
import Chip from "@mui/material/Chip";
import IconButton from "@mui/material/IconButton";
import CardHeader from "@mui/material/CardHeader";
import MoreVertIcon from "@mui/icons-material/MoreVert";

import DataLabel from "components/common/DataLabel";
import notEmptyString from "utils/notEmptyString"
import Title from "components/common/Title";
import Subtitle from "components/common/Subtitle";
import CardMenu from "components/common/CardMenu";

function UserCard(props) {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <CardMenu
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        actions={props.actions}
      />
      <Card sx={{ minWidth: 275 }}>
        <CardHeader
          action={
            props.actions.length > 0 && (
              <IconButton aria-label="options" onClick={handleClick}>
                <MoreVertIcon />
              </IconButton>
            )
          }
          title={
            <>
              <Title loading={props.loading} paragraph={false}>
                {notEmptyString(props.user.name)}
              </Title>
            </>
          }
          subheader={
            <>
              <Subtitle loading={props.loading}>
                {notEmptyString(props.user.email)}
              </Subtitle>
              {!props.loading && (
                <Chip
                  label={notEmptyString(props.user.role, "Sin rol")}
                  color="primary"
                  size="small"
                />
              )}
            </>
          }
        />
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <DataLabel
                title="Link de CV"
                data={notEmptyString(props.user.cvLink)}
                loading={props.loading}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <DataLabel
                title="Nivel de Inglés"
                data={notEmptyString(props.user.englishLevel)}
                loading={props.loading}
              />
            </Grid>
            <Grid item xs={12}>
              <DataLabel
                title="Conocimientos Técnicos"
                data={notEmptyString(props.user.hardSkills)}
                loading={props.loading}
              />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </>
  );
}

export default UserCard;
