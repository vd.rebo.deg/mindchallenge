import React, { useEffect } from "react";
import { API } from "aws-amplify";
import { useSelector } from 'react-redux'
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import { useFormik } from "formik";

import useLoadingStatus from "hooks/useLoadingStatus";
import { createUserSchema, updateUserSchema } from "validator/yup/schemas";
import nop from "utils/nop";

async function createUser(userData) {
  const data = {
    body: userData,
  };
  return API.post("mindChallengeApi", "/v1/user", data);
}

async function updateUser(userData) {
  const data = {
    body: userData,
  };
  return API.put("mindChallengeApi", "/v1/user", data);
}

function checkRoles(userRoles = [], roleToHave = "") {
  if (roleToHave === "") return true;
  return userRoles.some((role) => role === roleToHave);
}

function UserDialog(props) {
  const [createLoading, _createUser] = useLoadingStatus(createUser);
  const [updateLoading, _updateUser] = useLoadingStatus(updateUser);
  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      password: "",
      role: "",
    },
    validationSchema: props.updateMode ? updateUserSchema : createUserSchema,
    onSubmit: async ({ role, ...values }, { resetForm }) => {
      if (props.updateMode) {
        let data = {
          id: props.userID,
          ...values,
          roles: [role],
        };
        delete data.password;

        const userUpdated = await _updateUser(data);
        props.onUserUpdated(userUpdated);
      } else {
        let data = {
          ...values,
          roles: [role],
        };

        const newUser = await _createUser(data);
        props.onUserCreated(newUser);
      }

      resetForm();
    },
  });
  const userRoles = useSelector(state => state.user.roles)


  useEffect(() => {
    if (props.open) {
      const userToUpdate = {
        name: props.user.name,
        email: props.user.email,
        role: props.user.role,
      };

      formik.setValues(userToUpdate);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.userID, props.open]);

  function handleOnCancel() {
    formik.resetForm();
    props.onCancel();
  }

  const isSAdmin = checkRoles(userRoles, "SADMIN");

  const loading = createLoading || updateLoading;
  const submitBtnText = props.updateMode ? "ACTUALIZAR" : "REGISTRAR";

  return (
    <Dialog open={props.open} onClose={props.onCancel}>
      <form onSubmit={formik.handleSubmit}>
        <DialogTitle>Nuevo Usuario</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Ingrese los datos requeridos para dar de alta un nuevo usuario.
          </DialogContentText>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoFocus
                margin="dense"
                id="name"
                name="name"
                label="Nombre completo"
                type="text"
                fullWidth
                variant="standard"
                value={formik.values.name}
                onChange={formik.handleChange}
                error={formik.touched.name && Boolean(formik.errors.name)}
                helperText={formik.touched.name && formik.errors.name}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                margin="dense"
                id="email"
                name="email"
                label="Email"
                type="email"
                fullWidth
                variant="standard"
                required
                disabled={props.updateMode}
                value={formik.values.email}
                onChange={formik.handleChange}
                error={formik.touched.email && Boolean(formik.errors.email)}
                helperText={formik.touched.email && formik.errors.email}
              />
            </Grid>
            {!props.updateMode && (
              <Grid item xs={12} sm={6}>
                <TextField
                  margin="dense"
                  id="password"
                  name="password"
                  label="Contraseña"
                  type="text"
                  fullWidth
                  variant="standard"
                  required
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  error={
                    formik.touched.password && Boolean(formik.errors.password)
                  }
                  helperText={formik.touched.password && formik.errors.password}
                />
              </Grid>
            )}

            <Grid item xs={12} sm={6}>
              <FormControl
                variant="standard"
                sx={{ m: 1, minWidth: 120 }}
                required
                fullWidth
                error={formik.touched.role && Boolean(formik.errors.role)}
              >
                <InputLabel id="demo-simple-select-standard-label">
                  Rol
                </InputLabel>
                <Select
                  labelId="demo-simple-select-standard-label"
                  id="demo-simple-select-standard"
                  name="role"
                  value={formik.values.role}
                  onChange={formik.handleChange}
                  label="Rol"
                >
                  <MenuItem value="GENERAL">General</MenuItem>
                  {isSAdmin && <MenuItem value="ADMIN">Administrador</MenuItem>}
                </Select>
                <FormHelperText>
                  {formik.touched.role && formik.errors.role}
                </FormHelperText>
              </FormControl>
            </Grid>

            <Grid item xs={12}>
              <FormHelperText>* Campos requeridos</FormHelperText>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleOnCancel} disabled={loading} type="reset">
            Cancelar
          </Button>
          <Box sx={{ m: 1, position: "relative" }}>
            <Button type="submit" disabled={loading}>
              {submitBtnText}
            </Button>
            {loading && (
              <CircularProgress
                size={24}
                sx={{
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  marginTop: "-12px",
                  marginLeft: "-12px",
                }}
              />
            )}
          </Box>
        </DialogActions>
      </form>
    </Dialog>
  );
}

UserDialog.defaultProps = {
  onUserCreated: nop,
  onUserUpdated: nop,
  updateMode: false,
  userID: null,
  user: {},
};

export default UserDialog;
