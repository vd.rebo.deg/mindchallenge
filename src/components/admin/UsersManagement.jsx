import { useState, useEffect } from "react";
import { API } from "aws-amplify";
import { useNavigate } from "react-router-dom";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import PersonAddAlt1Icon from "@mui/icons-material/PersonAddAlt1";
import { DataGrid } from "@mui/x-data-grid";
import dayjs from "dayjs";

import UserDialog from "components/admin/UserDialog";
import useLoadingStatus from "hooks/useLoadingStatus";

async function fetchUsers() {
  return await API.get("mindChallengeApi", "/v1/user");
}

const COLUMNS = [
  {
    field: "name",
    headerName: "Nombre",
    width: 200,
  },
  {
    field: "email",
    headerName: "Email",
    width: 200,
  },
  {
    field: "role",
    headerName: "Rol",
    width: 100,
    valueGetter: (params) => params.row.roles[0] || "Sin info",
  },
  {
    field: "updatedAt",
    headerName: "Última actualización",
    width: 150,
    valueGetter: (params) =>
      params.row.updatedAt
        ? dayjs(params.row.updatedAt).format("DD/MM/YYYY")
        : "Sin info",
  },
  {
    field: "createdAt",
    headerName: "Fecha de registro",
    width: 150,
    valueGetter: (params) =>
      params.row.createdAt
        ? dayjs(params.row.createdAt).format("DD/MM/YYYY")
        : "Sin info",
  },
];

function UsersManagement(props) {
  const [users, setUsers] = useState([]);
  const [userDialogOpen, setUserDialogOpen] = useState(false);
  const [loading, _initUsersList] = useLoadingStatus(initUsersList);

  const navigate = useNavigate();

  useEffect(() => {
    _initUsersList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function handleClickOpen() {
    setUserDialogOpen(true);
  }

  function handleCancelDialogBtn() {
    setUserDialogOpen(false);
  }

  function handleUserCreated(newUser) {
    setUserDialogOpen(false);
    setUsers((users) => [{ ...newUser }, ...users]);
  }

  async function initUsersList() {
    const users = await fetchUsers();
    setUsers([...users]);
    return;
  }

  function handleRowClick(userData) {
    navigate(`/admin/usuarios/${userData.id}`);
  }

  return (
    <>
      <UserDialog
        open={userDialogOpen}
        onCancel={handleCancelDialogBtn}
        onUserCreated={handleUserCreated}
      />
      <Paper>
        <Box p={2}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={9}>
              <Typography variant="h4" component="h1">
                Usuarios
              </Typography>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Button
                variant="contained"
                size="small"
                disableElevation
                startIcon={<PersonAddAlt1Icon />}
                onClick={handleClickOpen}
              >
                Registrar
              </Button>
            </Grid>
          </Grid>
        </Box>
        <div style={{ height: 400, width: "100%" }}>
          <DataGrid
            onRowClick={handleRowClick}
            rows={users}
            loading={loading}
            columns={COLUMNS}
            pageSize={10}
            rowsPerPageOptions={[10, 25, 50, 100]}
          />
        </div>
      </Paper>
    </>
  );
}

export default UsersManagement;
