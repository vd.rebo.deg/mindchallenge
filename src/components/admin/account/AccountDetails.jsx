import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import useLoadingStatus from "hooks/useLoadingStatus";
import * as accountApi from "api/account";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import useBooleanFlag from "hooks/useBooleanFlag";

import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import MoreVertIcon from "@mui/icons-material/MoreVert";

import AccountDialog from "components/admin/account/AccountDialog";
import DataLabel from "components/common/DataLabel";
import notEmptyString from "utils/notEmptyString";
import Title from "components/common/Title";
import CardMenu from "components/common/CardMenu";
import ConfirmationDialog from "components/common/ConfirmationDialog";
import TransferList from "components/common/TransferList";
import * as userApi from "api/user";
import { not } from "utils/set";
import { Box } from "@mui/system";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography,
} from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import nop from "utils/nop";

const COLUMNS = [
  {
    field: "name",
    headerName: "Nombre",
    width: 200,
  },
  {
    field: "email",
    headerName: "Email",
    width: 200,
  },
];

function AccountDetails(props) {
  const [accountData, setAccountData] = useState({});
  const [users, setUsers] = useState([]);
  const [team, setTeam] = useState([]);
  const [selectedUsers, setSelectedUsers] = useState([]);
  const navigate = useNavigate();
  const [assignDialogOpen, openAssignDialog, closeAssignDialog] =
    useBooleanFlag();
  const [updDialogOpen, openUpdDialog, closeUpdDialog] = useBooleanFlag();
  const [delDialogOpen, openDelDialog, closeDelDialog] = useBooleanFlag();
  let [loading, _fetchAccountData] = useLoadingStatus(fetchAccountData);
  let { accountID } = useParams();
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    if (accountID) {
      _fetchAccountData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  async function fetchAccountData() {
    const accountData = await accountApi.fetch(accountID);
    const { team } = await accountApi.fetchTeam(accountID);
    setAccountData({ ...accountData });
    setTeam(team);
    setSelectedUsers(team);

    let users = await userApi.fetchAll();

    const usersList = users.map((user) => [user.id, user]);
    const usersMap = new Map(usersList);

    team.forEach((member) => {
      usersMap.delete(member.id);
    });

    users = Array.from(usersMap.values());

    setUsers(users);
  }

  function handleAccountUpdated(updatedData) {
    setAccountData({ ...updatedData });
    closeUpdDialog();
  }

  async function handleDeleteConfirmation() {
    await accountApi.del(accountID);
    closeDelDialog();
    navigate(`/admin/cuentas`);
  }

  function handleSelectedUsers(selected) {
    setSelectedUsers(selected);
  }

  async function handleAssignationConfirm() {
    const teamIds = team.map((item) => item.id);
    const selectedIds = selectedUsers.map((item) => item.id);
    const usersToAdd = not(selectedIds, teamIds);
    const usersToRemove = not(teamIds, selectedIds);

    if (usersToAdd.length > 0) {
      await accountApi.assignTeamUsers(accountID, usersToAdd);
    }

    if (usersToRemove.length > 0) {
      await accountApi.removeTeamUsers(accountID, usersToRemove);
    }

    closeAssignDialog();

    if (usersToAdd.length > 0 || usersToRemove.length > 0) {
      await _fetchAccountData();
    }
  }

  let menuOptions = [
    {
      icon: <EditIcon fontSize="small" />,
      text: "Actualizar",
      divider: true,
      action: openUpdDialog,
    },
    {
      icon: <DeleteIcon fontSize="small" />,
      text: "Eliminar",
      action: openDelDialog,
    },
  ];

  return (
    <>
      <ConfirmationDialog
        open={delDialogOpen}
        onCancel={closeDelDialog}
        onConfirm={handleDeleteConfirmation}
      />
      <AccountDialog
        open={updDialogOpen}
        onCancel={closeUpdDialog}
        onAccountUpdated={handleAccountUpdated}
        accountID={accountID}
        account={accountData}
        updateMode
      />
      <CardMenu
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        actions={menuOptions}
      />
      <Card sx={{ minWidth: 275 }}>
        <CardHeader
          action={
            menuOptions.length > 0 && (
              <IconButton aria-label="options" onClick={handleClick}>
                <MoreVertIcon />
              </IconButton>
            )
          }
          title={
            <>
              <Title loading={loading} paragraph={false}>
                Cuenta: {notEmptyString(accountData.accountName)}
              </Title>
            </>
          }
        />
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <DataLabel
                title="Cliente"
                data={notEmptyString(accountData.clientName)}
                loading={loading}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <DataLabel
                title="Responsable"
                data={notEmptyString(accountData.operationsManager)}
                loading={loading}
              />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <Box mt={2}>
        <Paper>
          <CardHeader
            title={
              <Grid container>
                <Grid item xs={12} sm={9}>
                  <Typography variant="h5">Equipo Asignado</Typography>
                </Grid>
                <Grid item xs={12} sm={3}>
                  <Button
                    startIcon={<EditIcon />}
                    variant="contained"
                    size="small"
                    disableElevation
                    onClick={openAssignDialog}
                  >
                    Modificar Usuarios
                  </Button>
                </Grid>
              </Grid>
            }
          />
          <div style={{ height: 400, width: "100%" }}>
            <DataGrid
              rows={team}
              loading={loading}
              columns={COLUMNS}
              pageSize={10}
              rowsPerPageOptions={[10, 25, 50, 100]}
            />
          </div>

          <AssignationDialog
            open={assignDialogOpen}
            right={team}
            left={users}
            onChange={handleSelectedUsers}
            onConfirm={handleAssignationConfirm}
            onCancel={closeAssignDialog}
          />
        </Paper>
      </Box>
    </>
  );
}

function AssignationDialog(props) {
  return (
    <Dialog maxWidth="lg" open={props.open}>
      <DialogTitle>Asignación de usuarios</DialogTitle>
      <DialogContent dividers>
        <DialogContentText paragraph>
          Para asignar usuarios a esta cuenta transfieralos al dado derecho.
          Para remover usuarios, transfieralos al lado izquierdo.
        </DialogContentText>
        <TransferList
          right={props.right}
          left={props.left}
          onChange={props.onChange}
          onConfirm={props.onConfirm}
        />
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={props.onCancel}>
          Cancelar
        </Button>
        <Button onClick={props.onConfirm}>Guardar</Button>
      </DialogActions>
    </Dialog>
  );
}

AssignationDialog.defaultProps = {
  onChange: nop,
  onConfirm: nop,
};

export default AccountDetails;
