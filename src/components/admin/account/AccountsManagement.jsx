import { useState, useEffect } from "react";
import { API } from "aws-amplify";
import { useNavigate } from "react-router-dom";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import DomainAddIcon from "@mui/icons-material/DomainAdd";
import { DataGrid } from "@mui/x-data-grid";
import dayjs from "dayjs";

import AccountDialog from "components/admin/account/AccountDialog";
import useLoadingStatus from "hooks/useLoadingStatus";

async function fetchAccounts() {
  return await API.get("mindChallengeApi", "/v1/account");
}

const COLUMNS = [
  {
    field: "accountName",
    headerName: "Nombre",
    width: 200,
  },
  {
    field: "clientName",
    headerName: "Cliente",
    width: 200,
  },
  {
    field: "operationsManager",
    headerName: "Responsable",
    width: 200,
  },
  {
    field: "updatedAt",
    headerName: "Última actualización",
    width: 150,
    valueGetter: (params) =>
      params.row.updatedAt
        ? dayjs(params.row.updatedAt).format("DD/MM/YYYY")
        : "Sin info",
  },
  {
    field: "createdAt",
    headerName: "Fecha de registro",
    width: 150,
    valueGetter: (params) =>
      params.row.createdAt
        ? dayjs(params.row.createdAt).format("DD/MM/YYYY")
        : "Sin info",
  },
];

function AccountsManagement(props) {
  const [accounts, setAccounts] = useState([]);
  const [userDialogOpen, setUserDialogOpen] = useState(false);
  const [loading, _initUsersList] = useLoadingStatus(initAccountsList);

  const navigate = useNavigate();

  useEffect(() => {
    _initUsersList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function handleClickOpen() {
    setUserDialogOpen(true);
  }

  function handleCancelDialogBtn() {
    setUserDialogOpen(false);
  }

  function handleAccountCreated(newAccount) {
    setUserDialogOpen(false);
    setAccounts((accounts) => [{ ...newAccount }, ...accounts]);
  }

  async function initAccountsList() {
    const users = await fetchAccounts();
    setAccounts([...users]);
    return;
  }

  function handleRowClick(userData) {
    navigate(`/admin/cuentas/${userData.id}`);
  }

  return (
    <>
      <AccountDialog
        open={userDialogOpen}
        onCancel={handleCancelDialogBtn}
        onAccountCreated={handleAccountCreated}
      />
      <Paper>
        <Box p={2}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={9}>
              <Typography variant="h4" component="h1">
                Cuentas
              </Typography>
            </Grid>
            <Grid item xs={12} sm={3}>
              <Button
                variant="contained"
                size="small"
                disableElevation
                startIcon={<DomainAddIcon />}
                onClick={handleClickOpen}
              >
                Registrar
              </Button>
            </Grid>
          </Grid>
        </Box>
        <div style={{ height: 400, width: "100%" }}>
          <DataGrid
            onRowClick={handleRowClick}
            rows={accounts}
            loading={loading}
            columns={COLUMNS}
            pageSize={10}
            rowsPerPageOptions={[10, 25, 50, 100]}
          />
        </div>
      </Paper>
    </>
  );
}

export default AccountsManagement;
