import React, { useEffect } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import FormHelperText from "@mui/material/FormHelperText";
import { useFormik } from "formik";

import useLoadingStatus from "hooks/useLoadingStatus";
import { createAccountSchema } from "validator/yup/schemas";
import nop from "utils/nop";
import * as accountApi from "api/account";


function AccountDialog(props) {
  const [createLoading, _createAccount] = useLoadingStatus(accountApi.create);
  const [updateLoading, _updateAccount] = useLoadingStatus(accountApi.update);
  const formik = useFormik({
    initialValues: {
      accountName: "",
      clientName: "",
      operationsManager: "",
    },
    validationSchema: createAccountSchema,
    onSubmit: async (values, { resetForm }) => {
      if (props.updateMode) {
        let data = {
          ...values,
        };

        const accountUpdated = await _updateAccount(props.accountID, data);
        props.onAccountUpdated(accountUpdated);
      } else {
        let data = {
          ...values,
        };

        const newAccount = await _createAccount(data);
        props.onAccountCreated(newAccount);
      }

      resetForm();
    },
  });


  useEffect(() => {
    if (props.open) {
      const accountToUpdate = {
        accountName: props.account.accountName,
        clientName: props.account.clientName,
        operationsManager: props.account.operationsManager,
      };

      formik.setValues(accountToUpdate);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.accountID, props.open]);

  function handleOnCancel() {
    formik.resetForm();
    props.onCancel();
  }


  const loading = createLoading || updateLoading;
  const submitBtnText = props.updateMode ? "ACTUALIZAR" : "REGISTRAR";

  return (
    <Dialog open={props.open} onClose={props.onCancel}>
      <form onSubmit={formik.handleSubmit}>
        <DialogTitle>Nueva Cuenta</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Ingrese los datos requeridos para dar de alta un nuevo usuario.
          </DialogContentText>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoFocus
                margin="dense"
                id="accountName"
                name="accountName"
                label="Nombre de la Cuenta"
                type="text"
                fullWidth
                variant="standard"
                value={formik.values.accountName}
                onChange={formik.handleChange}
                error={formik.touched.accountName && Boolean(formik.errors.accountName)}
                helperText={formik.touched.accountName && formik.errors.accountName}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                margin="dense"
                id="clientName"
                name="clientName"
                label="Cliente"
                type="clientName"
                fullWidth
                variant="standard"
                required
                value={formik.values.clientName}
                onChange={formik.handleChange}
                error={formik.touched.clientName && Boolean(formik.errors.clientName)}
                helperText={formik.touched.clientName && formik.errors.clientName}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                margin="dense"
                id="operationsManager"
                name="operationsManager"
                label="Responsable de Operación"
                type="operationsManager"
                fullWidth
                variant="standard"
                required
                value={formik.values.operationsManager}
                onChange={formik.handleChange}
                error={formik.touched.operationsManager && Boolean(formik.errors.operationsManager)}
                helperText={formik.touched.operationsManager && formik.errors.operationsManager}
              />
            </Grid>

            <Grid item xs={12}>
              <FormHelperText>* Campos requeridos</FormHelperText>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleOnCancel} disabled={loading} type="reset">
            Cancelar
          </Button>
          <Box sx={{ m: 1, position: "relative" }}>
            <Button type="submit" disabled={loading}>
              {submitBtnText}
            </Button>
            {loading && (
              <CircularProgress
                size={24}
                sx={{
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  marginTop: "-12px",
                  marginLeft: "-12px",
                }}
              />
            )}
          </Box>
        </DialogActions>
      </form>
    </Dialog>
  );
}

AccountDialog.defaultProps = {
  onAccountCreated: nop,
  onAccountUpdated: nop,
  updateMode: false,
  accountID: null,
  account: {},
};

export default AccountDialog;
