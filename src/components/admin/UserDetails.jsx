import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { API } from "aws-amplify";
import { useAuthenticator } from "@aws-amplify/ui-react";
import UserDialog from "components/admin/UserDialog";
import UserCard from "components/common/UserCard";
import useLoadingStatus from "hooks/useLoadingStatus";
import fetchUser from "api/fetchUser";
import EditProfileDialog from "components/common/EditProfileDialog";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";

import ConfirmationDialog from "components/common/ConfirmationDialog";
import useBooleanFlag from "hooks/useBooleanFlag";

async function deleteUser(userID) {
  return API.del("mindChallengeApi", `/v1/user/${userID}`);
}

function UserDetails(props) {
  const [userData, setUserData] = useState({});
  const [userDialogOpen, setUserDialogOpen] = useState(false);
  const navigate = useNavigate();
  const [profDialogOpen, openProfDialog, closeProfDialog] = useBooleanFlag();
  const [delDialogOpen, openDelDialog, closeDelDialog] = useBooleanFlag();
  let [loading, _fetchUserData] = useLoadingStatus(fetchUserData);
  const profileID = useSelector((state) => state.user.id);
  let { userID } = useParams();
  const { user } = useAuthenticator((context) => [context.user]);

  useEffect(() => {
    if (userID || (user && user.username)) {
      _fetchUserData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  async function fetchUserData() {
    let id = userID;

    if (props.profile) {
      id = user.username;
    }

    const userData = await fetchUser(id);
    setUserData({ ...userData, role: userData.roles[0] });
  }

  function handleClickOpen() {
    setUserDialogOpen(true);
  }

  function handleCancelDialogBtn() {
    setUserDialogOpen(false);
  }

  function handleUserUpdated(updatedData) {
    setUserData({ ...updatedData, role: updatedData.roles[0] });
    if (props.profile) {
      closeProfDialog();
    } else {
      setUserDialogOpen(false);
    }
  }

  async function handleDeleteConfirmation() {
    closeDelDialog();
    await deleteUser(userID);
    navigate(`/admin/usuarios`);
  }

  let menuOptions = [];

  if (profileID !== userID) {
    menuOptions = [
      {
        icon: <EditIcon fontSize="small" />,
        text: "Actualizar",
        divider: true,
        action: props.profile ? openProfDialog : handleClickOpen,
      },
    ];

    if (!props.profile && profileID !== userID) {
      menuOptions.push({
        icon: <DeleteIcon fontSize="small" />,
        text: "Eliminar",
        action: openDelDialog,
      });
    }
  }

  return (
    <>
      <ConfirmationDialog
        open={delDialogOpen}
        onCancel={closeDelDialog}
        onConfirm={handleDeleteConfirmation}
      />
      <EditProfileDialog
        open={profDialogOpen}
        user={userData}
        onCancel={closeProfDialog}
        onUserUpdated={handleUserUpdated}
      />
      <UserDialog
        open={userDialogOpen}
        onCancel={handleCancelDialogBtn}
        onUserUpdated={handleUserUpdated}
        userID={userID}
        user={userData}
        updateMode
      />
      <UserCard
        user={userData}
        loading={loading}
        actions={menuOptions}
        onEditBtnClick={props.profile ? openProfDialog : handleClickOpen}
      />
    </>
  );
}

export default UserDetails;
