import * as Yup from "yup";

const createUserSchema = Yup.object({
  name: Yup.string(),
  password: Yup.string()
    .min(8, "Longitud mínima de 8 caracteres")
    .max(20, "Longitud mínima de 8 caracteres")
    .matches(
      new RegExp("^[a-zA-Z0-9!#$%)(._-]{8,20}$"),
      "Debe contener minúsculas, mayúsculas, números y un caractér especial #$%._-"
    )
    .required("Requerido"),
  email: Yup.string().email("Email inválido").required("Requerido"),
  role: Yup.string().required("Requerido"),
});

const updateUserSchema = Yup.object({
  name: Yup.string(),
  email: Yup.string().email("Email inválido").required("Requerido"),
  role: Yup.string().ensure().required("Requerido"),
});

const updateProfileSchema = Yup.object({
  name: Yup.string(),
  englishLevel: Yup.string(),
  hardSkills: Yup.string(),
  cvLink: Yup.string().url(),
});

const createAccountSchema = Yup.object({
  accountName: Yup.string().required("Requerido"),
  clientName: Yup.string().required("Requerido"),
  operationsManager: Yup.string().required("Requerido"),
});

export { createUserSchema, updateUserSchema, updateProfileSchema, createAccountSchema };
