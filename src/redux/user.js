import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "user",
  initialState: {
    id: "",
    name: "",
    email: "",
    roles: [],
  },
  reducers: {
    setUser: (state, action) => {
        state.id = action.payload.id;
      state.email =  action.payload.email;
      state.name =  action.payload.name;
      state.roles =  action.payload.roles;
    },
  },
});

export const { setUser } = userSlice.actions;
export default userSlice.reducer;
