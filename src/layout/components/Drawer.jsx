import * as React from "react";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import PeopleIcon from "@mui/icons-material/People";
import BusinessIcon from "@mui/icons-material/Business";
import ManageSearchIcon from "@mui/icons-material/ManageSearch";
import PowerSettingsNewIcon from "@mui/icons-material/PowerSettingsNew";
import { NavLink } from "react-router-dom";
import { signOut } from "../../utils/session";

export const drawerWidth = 240;

const DRAWER_SECTION_ITEMS = [
  {
    text: "Usuarios",
    icon: PeopleIcon,
    to: "/admin/usuarios",
  },
  {
    text: "Cuentas",
    icon: BusinessIcon,
    to: "/admin/cuentas",
  },
  {
    text: "Logs",
    disabled: true,
    icon: ManageSearchIcon,
    to: "/admin/movimientos",
  },
];

const DRAWER_SESION_ITEMS = [
  {
    text: "Perfil",
    icon: AccountCircleIcon,
    to: "/perfil",
  },
  {
    text: "Cerrar Sesión",
    icon: PowerSettingsNewIcon,
    onClick: () => {
      signOut();
    },
  },
];

function CustomDrawer(props) {
  const { window } = props;

  const drawer = (
    <Box sx={{ flexGrow: 1, display: "flex", flexDirection: "column" }}>
      <Toolbar />
      <Divider />
      <List>
        {DRAWER_SECTION_ITEMS.map((item, index) => (
          <ListItem key={item.text} disablePadding>
            <ListItemButton
              component={NavLink}
              to={item.to}
              disabled={item.disabled}
            >
              <ListItemIcon>
                <item.icon />
              </ListItemIcon>
              <ListItemText primary={item.text} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      <Box sx={{ flexGrow: 1 }} />

      <Divider />
      <List>
        {DRAWER_SESION_ITEMS.map((item, index) => (
          <ListItem key={item.text} disablePadding>
            <ListItemButton
              onClick={item.onClick}
              component={NavLink}
              to={item.to}
              disabled={item.disabled}
            >
              <ListItemIcon>
                <item.icon />
              </ListItemIcon>
              <ListItemText primary={item.text} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <Box
      component="nav"
      sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
      aria-label="mailbox folders"
    >
      {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
      <Drawer
        container={container}
        variant="temporary"
        open={props.mobileOpen}
        onClose={props.handleDrawerToggle}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
        sx={{
          display: { xs: "block", sm: "none" },
          "& .MuiDrawer-paper": {
            boxSizing: "border-box",
            width: drawerWidth,
          },
        }}
      >
        {drawer}
      </Drawer>
      <Drawer
        variant="permanent"
        sx={{
          display: { xs: "none", sm: "block" },
          "& .MuiDrawer-paper": {
            boxSizing: "border-box",
            width: drawerWidth,
          },
        }}
        open
      >
        {drawer}
      </Drawer>
    </Box>
  );
}

export default CustomDrawer;
